package com.example.fruitmemorygamebo

import android.util.Log
import androidx.lifecycle.ViewModel


class GameViewModel : ViewModel() {

    var totesCartesCorrectes = false

    var imatges = arrayOf(
        R.drawable.apple,
        R.drawable.grapes,
        R.drawable.pineapple,
        R.drawable.apple,
        R.drawable.grapes,
        R.drawable.pineapple
    )
    var cartes = mutableListOf<Carta>()
    var cartaBuida = Carta(-1, -1)
    var cartesGirades = mutableListOf(cartaBuida, cartaBuida)
    var comptadorParelles = 0

    init {
        setDataModel()
    }

    private fun setDataModel() {
        imatges.shuffle()
        for (i in 0..5) {
            cartes.add(Carta(i, imatges[i]))
        }
    }

    var parellaIncorrecta = false

    fun parellaIncorrecta(){
        parellaIncorrecta = true
    }

    var comptadorClicks = 0

    fun girarCarta(idCarta: Int): Int {
        //comptadorClicks++
        if (!cartes[idCarta].girada) {
            cartes[idCarta].girada = true


            if (cartesGirades[0] === cartaBuida) {
                cartesGirades[0] = cartes[idCarta]

            } else {
                cartesGirades[1] = cartes[idCarta]

                if (compararCartes(cartesGirades[0], cartesGirades[1])) {
                    Log.d("Iguals", "Iguals")
                    comptadorParelles++
                    Log.d("parelles", "Comptador parelles fetes: "+comptadorParelles)
                    resetCarta()


                } else {
                    Log.d("No son Iguals", "No son Iguals")
                    retornarCarta1DelReves(cartesGirades[0].id)
                    retornarCarta2DelReves(cartesGirades[1].id)
                    resetCarta()

                    return R.drawable.cardreverse

                }
            }

            return cartes[idCarta].resId
        } else {
            cartes[idCarta].girada = false
            return R.drawable.cardreverse
        }
    }

    fun girarCartaV2(idCarta: Int): Int {

        if (!cartes[idCarta].girada) {
            cartes[idCarta].girada = true

            return cartes[idCarta].resId


        } else {
            cartes[idCarta].girada = false
            return R.drawable.cardreverse
        }
    }

    fun retornarCarta1DelReves(idCarta: Int): Int{
        cartes[idCarta].girada = false
        return R.drawable.cardreverse
    }

    fun retornarCarta2DelReves(idCarta: Int): Int{
        cartes[idCarta].girada = false
        return R.drawable.cardreverse
    }

    /*
    fun resetEstatJoc() {
        for (i in 0..5) {
            cartes[i].girada = false
        }
    }
    */

    fun estatCarta(idCarta: Int): Int {
        if (cartes[idCarta].girada) return cartes[idCarta].resId
        else return R.drawable.cardreverse
    }

    fun isMirantAmunt(id: Int): Boolean{
        return cartes[id].girada
    }

    fun compararCartes(id1: Carta, id2: Carta): Boolean {
        if (id1.resId == id2.resId){
            return true
        }
        return false
    }

    fun resetCarta() {
        cartesGirades[0] = cartaBuida
        cartesGirades[1] = cartaBuida
    }

}