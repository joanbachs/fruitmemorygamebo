package com.example.fruitmemorygamebo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class ResultActivity : AppCompatActivity() {

    private lateinit var score: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.result_screen)

        val botonPlayAgain = findViewById<Button>(R.id.playAgainButton)

        botonPlayAgain.setOnClickListener {
            val intentPlay = Intent(this, GameActivity::class.java)
            startActivity(intentPlay)
        }

        val botonMenu = findViewById<Button>(R.id.menuButton)

        botonMenu.setOnClickListener {
            val intentPlay = Intent(this, MainActivity::class.java)
            startActivity(intentPlay)
        }


        val extra: String? = intent.getStringExtra("comptadorClicks")
        score = findViewById(R.id.resultText)
        score.text = extra
    }
}