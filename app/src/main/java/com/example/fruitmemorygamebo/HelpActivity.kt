package com.example.fruitmemorygamebo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class HelpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.help_screen)


        val botonBack = findViewById<Button>(R.id.backButton)

        botonBack.setOnClickListener {
            val intentPlay = Intent(this, MainActivity::class.java)
            startActivity(intentPlay)
        }


    }
}