package com.example.fruitmemorygamebo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class GameActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView
    private lateinit var moviments: TextView

    private lateinit var viewModel: GameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_screen)

        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)

        carta1 = findViewById(R.id.imageButton)
        carta2 = findViewById(R.id.imageButton2)
        carta3 = findViewById(R.id.imageButton3)
        carta4 = findViewById(R.id.imageButton4)
        carta5 = findViewById(R.id.imageButton5)
        carta6 = findViewById(R.id.imageButton6)

        moviments = findViewById(R.id.numeroMoviments)

        carta1.setOnClickListener(this)
        carta2.setOnClickListener(this)
        carta3.setOnClickListener(this)
        carta4.setOnClickListener(this)
        carta5.setOnClickListener(this)
        carta6.setOnClickListener(this)

        updateUI()

    }

    override fun onClick(v: View?) {
        viewModel.comptadorClicks++

        when (v) {
            carta1 -> girarCarta(0, carta1)
            carta2 -> girarCarta(1, carta2)
            carta3 -> girarCarta(2, carta3)
            carta4 -> girarCarta(3, carta4)
            carta5 -> girarCarta(4, carta5)
            carta6 -> girarCarta(5, carta6)
        }
        Log.d("mov", viewModel.comptadorClicks.toString())
        moviments.setText(viewModel.comptadorClicks.toString())
    }


    private fun girarCarta(idCarta: Int, carta: ImageView) {
        if (viewModel.isMirantAmunt(idCarta)){
            Log.d("a", "Ja esta girada")
        }else{
            carta.setImageResource(viewModel.girarCarta(idCarta))
        }

        if (viewModel.comptadorParelles >= 3){
            viewModel.totesCartesCorrectes = true

        }

        if (viewModel.totesCartesCorrectes){
            val intentResultActivity = Intent (this, ResultActivity::class.java)
            intentResultActivity.putExtra("comptadorClicks", viewModel.comptadorClicks.toString())

            startActivity(intentResultActivity)

        }
    }


    private fun updateUI() {
        carta1.setImageResource(viewModel.estatCarta(0))
        carta2.setImageResource(viewModel.estatCarta(1))
        carta3.setImageResource(viewModel.estatCarta(2))
        carta4.setImageResource(viewModel.estatCarta(3))
        carta5.setImageResource(viewModel.estatCarta(4))
        carta6.setImageResource(viewModel.estatCarta(5))

    }


}